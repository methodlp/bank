<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 19.02.16
 * Time: 21:51
 */

namespace AppBundle\Entity\Interfaces;


interface TimestampableInterface
{
    public function setCreatedAt($createdAt);
    public function getCreatedAt();
    public function setUpdatedAt($updatedAt);
    public function getUpdatedAt();
}