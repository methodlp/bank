<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Account;
use AppBundle\Entity\Transaction;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Provider\DateTime;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Customer;

class LoadDefaultData implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $customer = new Customer();
        $customer->setUsername('admin');
        $customer->setPlainPassword('password');
        $customer->setIsAdmin(true);
        $customer->setPassword($this->generatePassword($customer));
        $manager->persist($customer);


        $customer = new Customer();
        $customer->setUsername('customer');
        $customer->setPlainPassword('password');
        $customer->setPassword($this->generatePassword($customer));

        $account = new Account();
        $account->setName('Default');
        $account->setBalance(10000);

        $customer->addAccount($account);

        $manager->persist($customer);

        $customer2 = new Customer();
        $customer2->setUsername('customer2');
        $customer2->setPlainPassword('password');
        $customer2->setPassword($this->generatePassword($customer2));

        $account2 = new Account();
        $account2->setName('Default');
        $account2->setBalance(10000);

        $customer2->addAccount($account2);

        $manager->persist($customer2);

        $manager->flush();

       $transferService = $this->container->get('app.transfer_service');


        for ($i = 0; $i < 100; $i++) {
            $transaction = new Transaction();
            $transaction->setAmount(rand(10,100));
            $transaction->setCreatedAt(DateTime::dateTimeBetween($startDate = '-10 years', $endDate = 'now'));
            $transaction->setUpdatedAt($transaction->getCreatedAt());
            $transaction->setAccount((($i%2 == 0) ? $account2 : $account));
            $transaction->setDescription('Some description');

            $transferService->processTransfer($transaction, (($i%2 == 0) ? $account : $account2));
        }


    }

    private function generatePassword($customer)
    {
        return $this->container->get('security.password_encoder')
            ->encodePassword($customer, $customer->getPlainPassword());
    }
}
