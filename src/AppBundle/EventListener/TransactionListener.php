<?php

namespace AppBundle\EventListener;


use AppBundle\Entity\Transaction;
use Doctrine\ORM\Event\LifecycleEventArgs;

class TransactionListener
{
    /**
     * Pre persist event
     *
     * @param LifecycleEventArgs $args Arguments
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if(!$entity instanceof Transaction) {
            return;
        }

        $currentBalance = $entity->getAccount()->getBalance();
        if ($entity->getId() === null) {



            if(in_array($entity->getType(), array(Transaction::DEBIT, Transaction::REFUND))) {
                $balance = $currentBalance - $entity->getAmount();
            }
            else {
                $balance = $currentBalance + $entity->getAmount();
            }

            $entity->getAccount()->setBalance($balance);
            $entity->setBalance($balance);

        }
    }

}