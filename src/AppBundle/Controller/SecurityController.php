<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 18.02.16
 * Time: 22:22
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security_login_form")
     */
    public function loginAction() {

        /**
         * @var $helper AuthenticationUtils
         */
        $helper = $this->get('security.authentication_utils');

        return $this->render('security/login.html.twig', array(
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ));
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction() {
        throw new \Exception('This should never be reached!');
    }

    /**
     * @Route("/login_check", name="security_login_check")
     */
    public function checkAction() {
        throw new \Exception('This should never be reached!');
    }
}