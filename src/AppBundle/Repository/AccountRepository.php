<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AccountRepository extends EntityRepository
{
    public function findCustomerAccounts($customer) {
       
        return $this->createQueryBuilder('account')
            ->andWhere('account.customer = :customer')
            ->setParameter('customer', $customer)
            ->getQuery()
            ->getArrayResult();
    }

    public function getCustomerAccounts($customer) {
        return $this->createQueryBuilder('account')
            ->where('account.customer = :customer')
            ->setParameter('customer', $customer);
    }

    public function getAccountsByCustomerUsername($username) {
        return $this->createQueryBuilder('account')
            ->join('account.customer', 'customer')
            ->select('account, customer')
            ->where('customer.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getArrayResult();
            ;
    }


}
