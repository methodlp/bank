<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Interfaces\TimestampableInterface;
use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="bank_account_transfers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransactionRepository")
 */
class Transaction implements TimestampableInterface
{
    use TimestampableTrait;

    CONST DEBIT = 'dr';
    CONST CREDIT = 'cr';
    CONST REFUND = 'ref';
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $description;

    /**
     * @ORM\Column(type="decimal")
     * @Assert\NotBlank(message="Please input an amount.", groups={"new"})
     * @Assert\Range(min = 0, minMessage="Please input valid amount.", groups={"new"})
     */
    protected $amount;

    /**
     * @ORM\Column(type="float")
     */
    protected $balance;

    /**
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="transactions", cascade={"persist"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     * @Assert\NotBlank(message="Please select an account.", groups={"new"})
     */
    protected $account;

    /**
     * @var string $type debit|credit
     *
     * @ORM\Column(name="type", type="string", length=25, nullable=false)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Transaction")
     * @ORM\JoinColumn(name="parent_transaction_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $parent_tx;


    public function __clone() {
        if ($this->id) {
            $this->setId(null);
        }
    }


    private function setId($id) {
        $this->id = $id;
    }
    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set account
     *
     * @param Account $account
     *
     * @return Transaction
     */
    public function setAccount(Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }


    /**
     * Set parentTx
     *
     * @param Transaction $parentTx
     *
     * @return Transaction
     */
    public function setParentTx(Transaction $parentTx = null)
    {
        $this->parent_tx = $parentTx;

        return $this;
    }

    /**
     * Get parentTx
     *
     * @return Transaction
     */
    public function getParentTx()
    {
        return $this->parent_tx;
    }

    /**
     * Set balance
     *
     * @param float $balance
     *
     * @return Transaction
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Transaction
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
