<?php

namespace AppBundle\Form;

use AppBundle\Entity\Customer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Proxies\__CG__\AppBundle\Entity\BankAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class TransactionType extends AbstractType
{
    private $tokenStorage;

    private $em;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManager $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = $this->tokenStorage->getToken()->getUser();

        if (!$user) {
            throw new \LogicException(
                'You must be an authenticated user!'
            );
        }

        $builder

            ->add('receiver', TextType::class, array(
                'label' => 'Search for user',
                'mapped' => false,
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Please input a username.'))
                ),

            ))
            ->add('search', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-info'
                )
            ));


            $addReceiverAccounts = function(Form $form, Customer $customer, Customer $user) {

                $accounts = null === $customer ? array() : $customer->getAccounts();
                $userAccounts = null === $user ? array() : $user->getAccounts();

                $form->add('account', EntityType::class, array(
                    'label' => sprintf('Choose one of %s`s accounts', $customer->getUsername()),
                    'class'       => 'AppBundle\Entity\Account',
                    'placeholder' => '-- Please select an account --',
                    'choices'     => $accounts,
                    'choice_label' => function ($choice) {
                        return $choice->getName() . ' (Current Balance: ' . $choice->getBalance() . ')';
                    },
                    'constraints' => array(
                        new NotBlank(array(
                            'groups' => array(
                                'new'
                            ),
                            'message' => 'Please select an account.'
                        ))
                    )
                ));

                $form->add('fromAccount', EntityType::class, array(
                    'label' => 'Choose one of your accounts',
                    'class'       => 'AppBundle\Entity\Account',
                    'placeholder' => '-- Please select an account --',
                    'mapped' => false,
                    'choices'     => $userAccounts,
                    'choice_label' => function ($choice) {
                        return $choice->getName() . ' (Current Balance: ' . $choice->getBalance() . ')';
                    },
                ));

                $form->add('amount', NumberType::class, array(
                    'label' => 'Specify amount of transaction',
                    'rounding_mode' => 2,
                    'attr' => array(
                        'data-behaviour' => 'formated_decimal_2'
                    )
                ));

                $form->add('submit', SubmitType::class);

            };

            $builder->get('receiver')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($addReceiverAccounts, $user) {
                    $receiver = $event->getForm()->getData();

                    if($user->getUsername() == strtolower($receiver)) {
                        $event
                            ->getForm()
                            ->getParent()
                            ->addError(new FormError('Currently transactions between your own accounts is unsupported'));
                    return;
                    }

                    $receiverResult = $this->em->getRepository('AppBundle:Customer')
                        ->createQueryBuilder('u')
                        ->where('u.username = :username')
                        ->setParameter('username', $receiver)
                        ->getQuery()
                        ->getOneOrNullResult();

                    if(!empty($receiverResult)) {
                        $addReceiverAccounts($event->getForm()->getParent(), $receiverResult, $user);
                    }
                    else {
                        $event
                            ->getForm()
                            ->getParent()
                            ->addError(new FormError('User does not exist!'));
                    }
                }
            );




    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=>array('novalidate'=>'novalidate'),
            'data_class' => 'AppBundle\Entity\Transaction',
            'allow_extra_fields' => true,
            'validation_groups' => function (FormInterface $form) {
                $data = $form->getData();

               if($form->has('submit') && $form->get('submit')->isClicked()) {
                   return 'new';
               }

            },
        ));
    }

}