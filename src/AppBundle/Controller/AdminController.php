<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new HttpException(403, "Looks like do not have access to this resource. :/");
        }
        $em = $this->getDoctrine()->getManager();
        $search = $request->get('s');
        $accounts = array();

        if($search) {
            $accounts = $em->getRepository('AppBundle:Account')->getAccountsByCustomerUsername($search);
        }

        if($request->isMethod('POST') && $request->get('account')) {
            $transferService = $this->container->get('app.transfer_service');
            $updates = $request->get('account');

            $updatedAccount = array_filter($updates, function($update) {
                return !empty($update['amount']) && array_key_exists('method', $update);
            });
            $updatedAccount = reset($updatedAccount);

            $account = $em->getRepository('AppBundle:Account')->find($updatedAccount['id']);

            $transaction = new Transaction();
            $transaction->setAccount($account);
            $transaction->setAmount($updatedAccount['amount']);
            if($updatedAccount['method'] == 'Add') {
                $transaction->setDescription('Added funds by admin');
                $transaction->setType(Transaction::CREDIT);

            }
            else {
                $transaction->setDescription('Removed funds by admin');
                $transaction->setType(Transaction::DEBIT);
            }

            if($transaction->getType() == Transaction::DEBIT && !$transferService->hasEnoughBalance($account->getBalance(), $transaction->getAmount())) {
                $this->addFlash('notice', 'The customer does not have enough funds.');
            }
            else {
                $transferService->processTransfer($transaction);
                $this->addFlash('notice', 'Account was updated successfully.');
                return $this->redirectToRoute('admin', array('s' => $search));
            }



        }

        return $this->render('admin/index.html.twig', array(
            'search' => $search,
            'accounts' => $accounts
        ));
    }
}
