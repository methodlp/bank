<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 19.02.16
 * Time: 21:47
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\Interfaces\TimestampableInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;


class TimestampableListener
{
    /**
     * Pre persist event
     *
     * @param LifecycleEventArgs $args Arguments
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof TimestampableInterface) {
            if($entity->getCreatedAt() != null) {
                return;
            }
            $now = new \DateTime('now');
            $entity->setCreatedAt($now)->setUpdatedAt($now);
        }
    }

    /**
     * Pre update event
     *
     * @param LifecycleEventArgs $args Arguments
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof TimestampableInterface) {
            $entity->setUpdatedAt(new \DateTime('now'));
        }
    }
}