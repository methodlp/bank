<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 20.02.16
 * Time: 11:02
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\Account;
use AppBundle\Entity\Customer;
use Doctrine\ORM\Event\LifecycleEventArgs;

class CustomerListener
{
    /**
     * Pre persist event
     *
     * @param LifecycleEventArgs $args Arguments
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        /**
         * Check if the customer has account already defined. If not make a default one.
         */
        if ($entity instanceof Customer && $entity->getId() == null && $entity->getIsAdmin() == false && $entity->getAccounts()->count() == 0) {

            $account = new Account();
            $account->setName('Default Account');

            $entity->addAccount($account);
        }
    }
}