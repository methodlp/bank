<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
    public function getTransactionsForAccountQuery($accountId)
    {
        return $this
            ->createQueryBuilder('transaction')
            ->leftJoin('transaction.parent_tx','p')
            ->select('transaction, p')

            ->where('transaction.account = :account')
            ->setParameter(':account', $accountId);
    }


}