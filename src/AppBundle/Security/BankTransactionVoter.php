<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 22.02.16
 * Time: 21:00
 */

namespace AppBundle\Security;


use AppBundle\Entity\Transaction;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BankTransactionVoter extends Voter
{
    CONST REFUNDABLE = 'TRANSFER_REFUNDABLE';
    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        if ($attribute != $this::REFUNDABLE) {
            return false;
        }

        if (!$subject instanceof Transaction) {
            return false;
        }
        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /**
         * @var $subject Transaction
         */
        if($attribute == $this::REFUNDABLE  && $subject->getType() == Transaction::CREDIT && $subject->getParentTx() === null) {
            return true;
        }
        return false;
    }
}