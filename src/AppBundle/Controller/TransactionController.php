<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Service\TransferService;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Transaction;

/**
 * Transaction controller.
 *
 */
class TransactionController extends Controller
{
    /**
     * @Route("/transactions", name="transactions_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $transactions = $em->getRepository('AppBundle:Transaction')->findAll();

        return $this->render('transaction/index.html.twig', array(
            'bankAccountTransactions' => $transactions,
        ));
    }

    /**
     * @Route("/account/{account}/transactions", name="transactions_for_account")
     * @Method("GET")
     */
    public function accountTransactionsAction(Request $request, Account $account) {

        $em = $this->getDoctrine()->getManager();

        $transactionsRepository = $em->getRepository('AppBundle:Transaction');


        $query = $transactionsRepository->getTransactionsForAccountQuery($account->getId());

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator
        
        ->paginate(
            $query,
            $request->query->getInt('page', 1),
            20,
            array(
                'defaultSortFieldName' => 'transaction.createdAt',
                'defaultSortDirection' => 'desc')
        );
        return $this->render('transaction/index.html.twig', array(
            'account' => $account,
            'bankAccountTransactions' => $pagination,
        ));

    }

    /**
     * Creates a new Transaction entity.
     *
     * @Route("/transactions/new", name="transactions_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $transaction = new Transaction();

        $form = $this->createForm('AppBundle\Form\TransactionType', $transaction);

        $form->handleRequest($request);


        if ($form->has('submit') && $form->get('submit')->isClicked() && $form->isValid()) {

            /**
             * @var TransferService
             */
            $transferService = $this->container->get('app.transfer_service');

            /**
             * @var $fromAccount Account
             */
            $fromAccount = $form->get('fromAccount')->getData();

            /**
             * @var $toAccount Account
             */
            $toAccount = $form->get('account')->getData();


            if($transferService->hasEnoughBalance($fromAccount->getBalance(), $transaction->getAmount())) {

                $transferService->processTransfer($transaction, $fromAccount);

                return $this->redirectToRoute('transactions_for_account', array(
                    'account' => $fromAccount->getId())
                );

            } else {
                $form->addError(new FormError('You do not have enough funds for this transaction'));
            }
        }

        return $this->render('transaction/new.html.twig', array(
            'bankAccountTransaction' => $transaction,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/refund/{transaction}", name="transactions_refund")
     * @Method({"GET"})
     * @param Transaction $transaction
     */
    public function refundAction(Transaction $transaction) {

         /**
         * @var TransferService
         */
        $transferService = $this->container->get('app.transfer_service');

        if($transferService->hasEnoughBalance($transaction->getAccount()->getBalance(), $transaction->getAmount())) {

            $transferService->processRefund($transaction);

            $this->addFlash(
                'notice',
                sprintf('Transaction #"%s" was refunded!', $transaction->getId())
            );

        } else {
            $this->addFlash(
                'notice',
                sprintf('Transaction #"%s" was not refunded! You do not have enough funds.', $transaction->getId())
            );
        }


        return $this->redirectToRoute('transactions_for_account', array('account' => $transaction->getAccount()->getId()));
    }

}
