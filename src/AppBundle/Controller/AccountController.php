<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Account;

/**
 * @Route("/accounts")
 */
class AccountController extends Controller
{
    /**
     * @Route("/", name="accounts_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $bankAccountsRepository = $em->getRepository('AppBundle:Account');

        /**
        * @var $customer \AppBundle\Entity\Customer
         */
        $customer = $this->get('security.token_storage')->getToken()->getUser();

        $bankAccounts = $bankAccountsRepository->findCustomerAccounts($customer);

        return $this->render('account/index.html.twig', array(
            'bankAccounts' => $bankAccounts,
        ));
    }

    /**
     * @Route("/new", name="accounts_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $account = new Account();
        $form = $this->createForm('AppBundle\Form\AccountType', $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /**
             * @var $customer \AppBundle\Entity\Customer
             */
            $customer = $this->get('security.token_storage')->getToken()->getUser();
            $account->setCustomer($customer);

            $em->persist($account);

            $em->flush();

            $this->addFlash(
                'notice',
            sprintf('Your new account "%s" was added!', $account->getName())
            );

            return $this->redirectToRoute('accounts_index');
        }

        return $this->render('account/new.html.twig', array(
            'bankAccount' => $account,
            'form' => $form->createView(),
        ));
    }


}
