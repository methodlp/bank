<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 21.02.16
 * Time: 15:15
 */

namespace AppBundle\Service;


use AppBundle\Entity\Account;
use AppBundle\Entity\Transaction;
use Doctrine\ORM\EntityManager;

class TransferService
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function hasEnoughBalance($currentBalance, $amount) {
        return $currentBalance >= $amount;
    }

    public function processTransfer(Transaction $transaction, Account $fromAccount = null) {


        if($transaction->getType() == null) {
            $transaction->setType(Transaction::CREDIT);

        }

        if(empty($transaction->getDescription())) {
            $transaction->setDescription('Transfer from A/C: ' . $fromAccount->getId());
        }

        $this->em->persist($transaction);

        if($fromAccount != null) {

        $fromTransaction = clone $transaction;

        $fromTransaction->setDescription('Transfer to A/C: ' . $transaction->getAccount()->getId());
        
        $fromTransaction->setType(Transaction::DEBIT);

        $fromTransaction->setParentTx($transaction);
        $fromTransaction->setAccount($fromAccount);

        $this->em->persist($fromTransaction);

        }



        $this->em->flush();
    }

    public function processRefund(Transaction $transaction) {

        $parent = $this->em->getRepository('AppBundle:Transaction')->findOneBy(array('parent_tx' => $transaction->getId()));

        $refund = new Transaction();
        $refund->setDescription('Refund for transaction #'.$transaction->getId());
        $refund->setAmount($transaction->getAmount());
        $refund->setAccount($transaction->getAccount());
        $refund->setType(Transaction::REFUND);
        $refund->setParentTx($transaction);

        $this->em->persist($refund);

        $transaction->setParentTx($refund);

        $this->em->persist($transaction);

        if($parent) {
            $parent->getAccount()->setBalance($parent->getAccount()->getBalance() + $refund->getAmount());
            $this->em->persist($parent);
        }

       $this->em->flush();
    }

}